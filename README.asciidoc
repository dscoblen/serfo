= Southeast Renaissance Fencing Open Weapon Rules
:title-page-background-image: image:Fechtschule.png[]
:doctype: book
:toc: left

ifdef::backend-html5[]
image::Fechtschule.png[scaledwidth="100%", align="center"]
endif::[]



[abstract]
== Purpose 
The Southeast Renaissance Fencing Open (SERFO) is a tournament event dedicated to promoting the practice and teaching of Historical European Martial Arts in the region. Most clubs in attendance are based in Georgia, Tennessee, North Carolina, South Carolina, and Florida. Participants come from a variety of backgrounds in terms of their choices of weapons, time periods, and manuals of study. SERFO is a multi-disciplinary event with a focus on Renaissance weapons, and we give equal footing to rapier and longsword fencing. SERFO is known as being a competitive, but friendly and open event. We endeavor every year to being welcoming and supportive to all fencers. While the fencing is at all times intense, and done with a desire to win, we view winning as a secondary aspect of fencing well. A victory for us is the fencer having fun, and improving their overall skill. We hope that you have fun, and wish to come back to SERFO again and again.

image::Capo.png[scaledwidth="100%", align="center"]

== Competition Area
* Bouts will take place in an 8m circle with clearly marked edges. 
* Fencers will start in one of two starting areas, red or yellow. 

.Equipment
* Table for staff - This will be positioned where the Director can easily see the timekeeper and the scoreboard.
* Scoreboard - digital or manual scoreboards are acceptable
* Timer – A digital timer clearly visible to Director and Competitors will be used.

== Minimum Equipment Safety Requirements
All equipment will be checked when a fencer registers on the day for their tournaments. No fencer shall be allowed to participate until all of their gear has been inspected and passed.

Participants are encouraged to contact the tournament organizers ahead of time if they have any questions regarding equipment. There may be limited equipment available the day of the tournament for participants to borrow. Some equipment may also be available for purchase on the day of the tournament.

=== Rapier
* Mask: 3-weapon mask in good condition with no discernible rusting, breaches of the mesh, tongue, or bib. The mask must completely cover the participant's ears and be neither too large nor small to snugly fit the competitor. Other headgear (i.e. steel helmets) will be allowed pending inspection for similar criteria as above. No electronic items are allowed to be added to the mask.
* Neck Protection: Some form of rigid neck protection or gorget that covers the larynx must be worn.
* Torso: A standard fencing jacket, or other jacket designed specifically for HEMA. Historical clothing designed to be worn for fencing is acceptable provided it meets minimum safety standards.
* Gloves: Gloves must be worn on both hands.
* Groin/Breast Protection: Use of rigid groin protection (cup/box) is mandatory for men. Women are highly recommended to wear rigid breast protection. Pelvic protectors are recommended for female participants. Rigid chest protection is recommended for all participants.
* Footwear: Fencers must wear some kind of enclosed toe footwear. Historical reproduction footwear is acceptable.
* No bare skin can be showing while fencing.

.The first blood Rapier tournament will require the following additional equipment
* Back of the head protection: Commercially manufactured back of the head protection is preferred, other methods will be accepted on a case-by-case basis
* Gloves: Fencers must wear gloves or gauntlets which protect the hand, fingers, and the portion of the wrist nearest the hand. Unpadded gloves are not sufficient.
* Elbow/Knee Protection: Use of rigid elbow and knee protection is mandatory. The protection must completely cover the knee and elbow.

=== Longsword and Sword and Buckler
* Mask: (Back-of-head protection required for steel) 3-weapon mask in good condition with no discernible rusting, breaches of the mesh, tongue, or bib. The mask must completely cover the participant's ears and be neither too large nor small to snugly fit the competitor. Other headgear (i.e. steel helmets) will be allowed pending inspection for similar criteria as above. No electronic items are allowed to be added to the mask.
* Protection for the back of the head is required for all participants.
* Neck Protection: Some form of rigid neck protection or gorget that covers the larynx must be worn.
* Gauntlets: Fencers must wear gloves or gauntlets which protect the hand, fingers, and the portion of the wrist nearest the hand. Gauntlets must have rigid and padded protection; unpadded gloves are not sufficient.
* Steel Tournament: SPES Heavy Gloves, Sparring gloves (Fechtschule Gdansk/Ensifers), Neyman Thokks, HF Armory, Thokk Gloves, Gabriel Gloves, St. Marks, ETC or modified lacrosse/hockey gloves with significant rigid exoskeleton are required. Contact us if you have any questions about the acceptability of your gloves. Gloves not on this list must be preapproved. No exceptions will be granted on the day of the tournament.
* Sword and Buckler Tournament - For the Buckler hand gloves which have no voids in protection on the back of the hand may be acceptable, but final determination will be on a case-by-case basis. Lacrosse/Ice hockey gloves are recommended.
* Groin/Breast Protection: Use of rigid groin protection (cup/box) is mandatory for men. Women are recommended to wear rigid breast protection. Pelvic protectors are recommended for female participants. Rigid chest protection is recommended for all participants.
* Jacket: Fencers must wear a padded jacket that completely covers the torso,back, and arms. Coaches fencing jackets or padded reproduction gambesons are recommended. For example, SPES AP or Fechtschule Gdansk Jackets, NEYMAN HEMA Jacket, PBT HEMA Jacket, etc.
* Elbow/Knee Protection: Use of rigid elbow and knee protection is mandatory. The protection must completely cover the knee and elbow.
* Footwear: Fencers must wear some kind of enclosed toe footwear. Historical reproduction footwear is acceptable.
* Shin: Rigid shin protection is recommended for nylon and mandatory for steel.
* No bare skin can be showing while fencing.

== Weapon requirements

=== Rapier
* Rapiers blades shall be between 35 and 45 inches in length from the crossbar to the tip.
* Dagger blades shall be no longer than 18 inches in length
* Only rapier and dagger blades intended for fencing will be allowed (Del Tin, Darkwood, Zen Warrior, Alchem, Hanwei, etc...) Others may be accepted, contact the tournament organizers for more details
* Flexi daggers are not allowed
* Rapiers and Daggers must be covered with plastic blunts, held in place with tape of contrasting color

=== Longsword
For the Steel Longsword tournament you shall be using your own sword. Acceptable weapons include the following:

* Albion Meyer
* Arms and Armor Fechtbuch
* Arms and Armor Fechterspiel
* Aureus Feder
* Black Fencer Feder
* Ensifer Feder (Any style)
* Regenyei Feder (Any style)
* Pavel Moc (Any style)
* Comfort Fencing (Any style)
* Castille Armory
* Sigi Armory
* Krieger
* VB/PHA Feders

Any other type of Feder style Longsword will be taken on a case by case basis. Please contact us with the type of sword before the tournament to get it cleared.

For the Steel Longsword tournament weapons that will not be allowed include blunt longswords, sharp longswords, fantasy styled longswords, etc. Examples include:

* Albion Liechtenauer
* Hanwei Practical Bastard
* Hanwei Tinker Pearce Blunt Trainer
* Arms and Armor Spada da Zogho


All Feder tips must be covered with plastic blunts/leather/etc, and held in place with tape of contrasting color

=== Sword and Buckler

For the Sword and Buckler tournament you shall be using your own sword. Sword brands are the same, though models must be either “side swords” or arming swords. Exceptions and edge-cases should be requested/reviewed as far in advance as possible.

*	Training Arming sword of any type are acceptable
*	Training Messers are acceptable. They must have a comparable flexibility to the Arming Swords and Side Swords allowed.
*	Training Side Swords are acceptable. A simple hand guard is allowed. Ports and Side Rings are allowed.
*	Maximum Blade length allowed is 38 inches.
*	Examples of Arming Swords:
**	Albion I:33
**	Arms and Armor Scholar Sword
*	Examples of Messers:
**	Arms and Armor Messer
**	Comfort Fencing Messer
**  Landsknecht Emporium 
*	Examples of Side Swords:
**	Darkwood Arms and Port
**	Darkwood 2 Port Cross


.Buckler requirements:
* Wooden or Steel Bucklers are acceptable
* Nylon Bucklers are acceptable
* Targa are acceptable
* Max width is 16 inches
* No miniumum width

== Rapier Tournament Rules

=== General Rules

A valid thrust requires controlled pressure in line with the point and should generally
cause the fencer’s blade to bend. Judges should note that some clearly valid thrusts
hit at such an angle that the blade does not bend, though. This will be called at the
judge’s discretion.

A valid cut requires controlled pressure in line with the edge. Incidental contact with
the edge will not be considered valid.

Hard hits are inevitable in any tournament, but fencers who consistently use
excessive force will be warned or penalized by the loss of a point or the loss of a
match. Excessively hard hits will not be penalized if the director feels the excessive hit
was the result of the receiving fencer's own action (i.e. lunging into the opponent's
attack).

If a fencer covers his or her valid target area with their arm or hand and a receives a
hit that would have landed on the body or head, the hit may be counted as having hit
the deeper target. The application of this rule is the discretion of the director.

=== Conventional Rapier
The rules for this tournament are loosely based on the conventions for competition
described by Giovanni Battista Gaiani in his 1619 text, _Arte di maneggiar la spada a
piedi et a cavallo_. These rules view the tournament bout as a demonstration of skill
rather than a simulation of a serious encounter. Each bout will be fought to 6 points,
or 2 minutes.

This tournament is single rapier.

.Scoring
* Arm: 1 point
* Body or Head: 3 points
* Below the waist: Not Counted

.Techniques allowed
* Only thrusts shall be used.

Double hits and after-blows will be scored as regular hits. Only the first hit for each
fencer will be counted during an exchange.

There shall be no grappling or in-fighting. If both fencers arrive at in-fighting measure without scoring a touch, a halt will be called and the fencers will reset.

If either fencer steps outside the boundaries of the fencing area, a halt will be called and the fencers will reset.

It is possible for both fencers to score 6 or more points against an opponent. If this happens during pool phase of the tournament, it will be counted as a loss for both fencers. If it occurs during the elimination rounds, the fencer with the highest score will win, but will begin the next round at a disadvantage based on the number points that were scored against him or her. This penalty shall not exceed 3 points.

=== First Blood Rapier
Unlike the conventional tournament, the first blood tournament is intended to more closely resemble a serious encounter. Bouts will be fought until a fencer scores one valid hit.

This tournament is Rapier and Dagger.

Each bout will consist of one 2 minute round. If there is no score at the end of the period, the director will flip a coin and assign priority to one of the fencers. The fencers will continue for one more minute. If neither has scored at the end, the fencer with priority will win the bout.

Each bout will be fenced until a fencer scores a single, valid hit. A valid hit can be delivered with either a cut or a thrust. A disarm will count as a hit. Light grappling (no throws) is allowed. Pommel strikes to the front of the mask only are allowed and are valid hits, but they must be pulled and should not land with full force.

A fencer can also lose by stepping outside of the boundaries of the fencing area with both feet. If both fencers leave the area it will be counted the same as a double hit. Fencers will not receive a warning when the near the boundaries of the fencing area, and will not be allowed the opportunity to reset.

In the pools round, a double hit or after blow will count as a loss for both fencers. In the elimination round, the bout will be re-fought.

== Longsword Tournament Rules

This is a Bring Your Own Sword event, but if you need to borrow a sword it may be possible. Please contact the event organizers before the event for arrangements. Bouts will be fought until a fencer has a score of 9 points, or time runs out. If the time runs out, the highest score will count. Length of bout is a continuous 3 minutes. 

The full rules for this event can be found here: https://drive.google.com/file/d/1YOiyCK3neiKiWwNo4u8JxsWJz1ZvzmNm/view

There shall be two tournaments held for Longsword.
* Open
* Women’s

It is possible to compete in both the Open and the Women’s tournaments.

=== Summary
Matches: Each match lasts 3 minutes in the pools and the eliminations.
The last exchange of the match is always at least 10 seconds—if an exchange ends with less than 10 seconds on the clock, 10 seconds are added and “Last Exchange” is called.
First fencer to 9 points wins the match ends.

Staff: The Judging team consists of two Directors. Each ring also has one or two Table Crew members and a Ring Boss.
The Director consults with the other Director on calls and they have the last word on how the match is conducted and scored.
The Table Crew runs the clock and manages all records of the match.
The Ring Boss manages the fighters and coaches and ensures the ring runs smoothly.

Exchanges: Each Exchange is judged as one of five types:

* Clean Hit: One fighter hits the other and is not hit in return.
* Failed Withdraw: One fighter hits the other but is hit afterward.
* Double-Hit: Both fighters hit each other at roughly the same time.
* Grapple: A ring-out, standing control with arms or sword; after a verbal 5-count, the grapple is stopped.
* No Exchange: Halt is called but the Judges determine that neither fighter hit.

>>>
Scoring: Each exchange is worth 0, 1, 2, or 4 points, as follows:


|==========================
^e| Points  ^e| Clean Hit ^e| Failed Withdrawal ^e| Grapple ^e| Double Hit
| *0* | Contact    | Contact - Quality | Unresolved            | +
| *1* | Quality    | Target++          | If valid action****   | -
| *2* | Target++   | -                 | If valid action****   | 
| *4* | Control+++ |                   | If valid action****   | 
|==========================

* A Double where the swords are in contact is called as Closed and thrown out. A Double where the swords are not in contact is called as Open and counts towards seeding.
** Target means a cut or thrust to the head, neck, or torso.
*** Control means one of three conditions:
A cut or thrust on Target while in the bind/on the sword. 	
A cut or thrust on Target while disabling the other weapon with a slice or grab.
If the Judges decide a Control technique is achieved, any strikes afterward are ignored.
**** Grappling in itself does not score. However, you can use it to set up scoring actions. Throws and Trips are strictly forbidden.



== Sword and Buckler Tournament Rules
This tournament aims to promote Sword and Buckler fencing from the German traditions up to the Bolognese traditions. Due to this it is possible for fencers to end up fencing somewhat ahistorical weapon matches such as late 14th century Arming Sword vs an early 16th century Side Sword. As this is the case we view this event as a way of highlighting the art that you study. We desire to see you fence as cleanly and as true to the sources you study as possible.

Bouts will be fought until a fencer reaches 9 points first, or with the highest score at the end of time shall win. Length of bout is a continous 2 minutes. This is done due to time constraints within the overall event.

.Valid Techniques
* Long edge cuts
* Short edge cuts
* Slicing cuts. The technique known as Slicing (Schneiden) used to block and control your opponent does not score. Only push or pull cuts with Long or Short edge.
* Thrusts
* Pommel Strikes

Strikes with the Buckler will not score and will result in a disqualification if thrown to the Head.

.Allowed Target areas plus score

* Thrust or cut to head - 3 points
* Thrust to body - 3 points
* Cut to body, and cut or thrust elsewhere - 1 point

.Doubles and Failed Withdrawals

A Double is where both Fencers strike each other within the same action.

* In the case of a Double no score shall be given.

A Bad or Failed Withdrawal (Abzug) is when a fighter lands a successful hit as given above, but fails to defend himself afterward.

* Once a hit is scored, the Director will allow a brief moment to elapse before the bout is stopped (roughly the time required to execute a single step), during which time the other fighter can attempt a “revenge strike”.
* If the hit is to the head or a thrust to the body: This "revenge strike" can only be against any part of the upper body ( from the hips up).
* Any fighter who Fails to Withdraw safely following a valid strike to the head shall receive only 1 point.
* Any fighter who Fails to Withdraw safely following a valid strike to the anywhere but the head shall receive no points.

== Sabre invitational Rules
This tournament is based on the rules found in Italian Fencing Event Rules of 1910 By Chris Holzman. This ruleset was the official ruleset of the Italian Fencing Federation. The SERFO ruleset is adapted from it to allow us to show a demonstration of skill rather than a simulation of a serious encounter. 

=== Running of a Bout

.Time	
* 90 seconds	
* The clock shall stop on each point		

.Valid targets 	
* Head			
* Torso			
* Arms			
				
.Valid attacks	
* Point			
* Edge			
* False edge			
* Blows on the blade, that is to say through the sword, do not count as valid			
				
.Point Value	
* 1 point no matter the target			
				
.Victory condition	
* Highest score			
* If there is a Tie then the First clean touch wins			
				
=== Doubles
The come in two criteria "Incontro" and "True Double"

==== Incontro
This is an at fault double, in which one of the fencers has made a mistake which caused the double touch. When the incontro occurs, the director will describe the sequence and assign "fault" to one of the fencers based on the below rules. The point will go to the fencer who was not at fault. 

**A fencer with 5 "at fault" doubles against them within a single pool bout will be disqualified from elimination rounds.**

The attacking fencer is considered to be at fault:

* When, with the opponent holding the blade in line, (with the point toward the face, chest, or flank, and the arm naturally extended), the attacker did not deflect the point with an action on the blade.

* When, seeking the opponent's blade, the attacker did not find it (make contact) and the opponent eluded the blade while counterattacking.

* When, in a counter-time action, the attacker strikes the opponent without having avoided the counterattack.

* When, in a compound action with feints, the defender found the attacker's blade during the feint, acquiring the right to riposte.

* When the attacker replaces or renews against the opponent's parry and immediate riposte.

The fencer who is on defense will instead be considered as touched:	

* When the defender delays the riposte, giving the opponent the right to remise.

* When the defender counterattacks without protecting themself from the final [action].


==== True double
This is a double touch that was a true simultaneous attack, where each fencer conceived and executed an attack at the same tempo. Neither can truly be said to be at fault for such a double touch. There is no penalty for a True Double, though it is much rarer than the Incontro. 

== Rapier

* Each bout will be officiated by a total of four judges and a Director.
* Two Judges will be assigned to only watch for hits against one fencer.
* The Director will be responsible for starting and stopping the bout, determining the validity of hits, and keeping track of score.
* When a judge observes a hit, he or she will raise the appropriate fencer's flag, and the Director will call halt. The Director may also halt the bout if he or she observes a hit.
* The Director will allow sufficient time after the initial hit for the possibility of an afterblow. The bout shall continue until either the attacker has retreated safely out of measure or immobilized the opponent so they are unable to strike. The defender is allowed no more than one step and one strike to attempt an afterblow.
* After the bout has been halted, the Director will call the action if necessary (multiple hits, out of bounds, etc...) The Director’s ruling on the action is final.
* The Director will then ask each judge if he or she observed a hit against their assigned fencer. Judges shall answer: Yes, No, Invalid (flat, light, off target, etc..), or Abstain.
* The Director can overrule one of the judges, but not both.
* In the event that the score is tied at the end of a period, the Director will flip a coin and assign priority to one of the fencers. The fencers will continue for one more minute. If neither has scored at the end, the fencer with priority will win the bout.
* A fencer may choose to acknowledge a hit made against him or her, or decline to accept a hit against their opponent at any time, but this is not required or expected. The Director may choose to accept this or not at his or her discretion.
* A fencer shall never attempt to call a hit against their opponent, or argue that an opponent’s hit was invalid.

== Longsword, Sword and Buckler and Sabre
* Each bout will be officiated by a total of two Directors, Head and Assistant.
* The Director will be responsible for starting and stopping the bout, determining the validity of hits, and keeping track of score.
* When a director observes a hit, they will raise the appropriate fencer's flag, and the Director will call halt. The Director may also halt the bout if they observes a hit.
* The Director will allow sufficient time after the initial hit for the possibility of an afterblow. The bout shall continue until either the attacker has retreated safely out of measure or immobilized the opponent so they are unable to strike. The defender is allowed no more than one step and one strike to attempt an afterblow.
* After the bout has been halted, the Director will call the action if necessary (multiple hits, out of bounds, etc...) The Director’s ruling on the action is final.
* A fencer may choose to acknowledge a hit made against them, or decline to accept a hit against their opponent after the Directors have made their call, but this is not required or expected. The Director may choose to accept this or not at their discretion.
* A fencer shall never attempt to call a hit against their opponent, or argue that an opponent’s hit was invalid.

== Injuries
* Fencers must act in a prudent and reasonable manner at all times in order to avoid injury to themselves, their opponents, tournament staff, and spectators.
* Any Fencer who acts in an unsafe or malicious manner may be disqualified from further participation in the tournament at the discretion of the tournament administrator.
* No Fencer, Coach, or Spectator will enter the arena to assist with an injury unless called for by the Director.
* An injured Fencer will be allowed a 2 minute period to decide if he or she is well enough to continue. If a Fencer is not well enough to continue, the match is forfeited. A warning may be issued to the injured Fencer's opponent if the injury was
caused by reckless, but not illegal or malicious, behavior.
* In all cases of injury, the judges and tournament administrator will make an assessment as to whether the injury was inflicted maliciously. A Fencer who injures his opponent intentionally shall be disqualified from the tournament.
* Injury inflicted on any other party (Director, judges administrative staff, spectators) may result in disqualification, depending on the circumstances and at the discretion of the tournament administrator

== Roles of the Staff

=== Director
* The Director starts and stops the fight, communicates with the Fencers, ensures all participants are ready, controls safety in the ring, interprets the actions, and announces points for the scorekeeper.
* Only the Fencer or Coach may communicate with the Director for clarification of a decision. 

=== Scorekeeper
* The Scorekeeper tracks the action of the match as reported by the Director, and updates the digital scoreboard for the match.
* Only tournament staff may communicate directly with the Scorekeeper during a bout; any Fencer or Coach who attempts to do so will receive a warning.
* Fencers or Coaches may communicate with the Scorekeeper during a break to ascertain bout order.

=== Ring Boss

* The Ring Boss controls the general running of the competition area.
* The Ring Boss prepares the next pair of Fencers for their bout.
* The Ring boss shall fill in the Fencer brackets.
* Fencers or Coaches may communicate with the Ring Boss about general information as long as they do not interfere with the running of a bout.

== Roles of the Fencers and Coaches
=== Fencers
* There will be two Fencers for every match.
* No more than two Fencers are allowed in the competition area at a time.
* Fencers must report to the mat when called.
* Any Fencer called to the competition area three times in a three-minute period without reporting will forfeit the match.

=== Coaches
* Each Fencer may be attended by one Coach.
* The Coach is allowed to give their Fencer advice throughout the bout.
* If the Coach is too loud and interfering with the bout then they shall be given a warning by the Director. Two such warnings will see the Coach being disqualified from the tournament. They shall not allowed to be next to the competition area for the rest of the event.
* The Coach may enter the mat if invited by the Director, but must otherwise stay outside of the ring and may not interfere with the Director, other Fencers, or any tournament staff.

== Prohibited Actions and Penalties:
Penalties are divided into 3 categories: Yellow Cards, Red Cards, and Black Cards. 

* Yellow Card 
This warning comes in 2 varieties
** First Yellow card: Warning and scoring action annulled
** Second and subsequent Yellow cards: 1 point to opponent and scoring action annulled
** Should a fencer continously receive Yellow cards the Director may instead give a Red card for continued infractions
* Red Card
Ejection from tournament
* Black Card
Ejection from all tournaments and events


In most cases a Yellow Card (warning) must be given before a Red Card is given (see exceptions below). Penalty cards do not carry over into other bouts.

=== Prohibited actions: 

The following actions and behaviors are prohibited, and disregard for the rules may result in penalization or expulsion from both individual tournament and the overall event.  

* Stalling -  Stalling refers to any action by a competitor which slows or delays the conduct of a bout in order to gain an advantage or stop the clock.  Examples include, but are not limited to delaying returning to the starting line when instructed by the referee, intentionally dropping the sword or falling down to stop the clock, intentionally stepping out of bounds, or feigning injury*. A competitor who intentionally steps out of bounds will receive the penalty for stalling in addition to the opponent receiving a point for the ring out.  A competitor who feigns injury may be penalized for cheating.

* False Start -  A false start occurs when a competitor moves from the starting line before the command of “Fight/Fence” is given by the referee.  

* Failure to halt* - Fighters must cease any and all attacks upon the first command of “Halt” from the referee.  Continue to attack or advance toward the opponent with the intention to attack after halt penalized.

* Influencing Referees - Fighters may acknowledge being hit, but are strictly forbidden from making any indication, verbal or non-verbal, about the location or quality of a hit given to their opponent in order to influence the referee’s call. 

* Interposition -  Interposition is when a fighter places a lower value target in the way of an oncoming strike to prevent a strike to a higher value target (e.g. blocks a cut to the head by putting the arm in the way of the attack), whether intentional or unintentional. In addition to the penalty card, the referee may award the higher value target at their discretion. 

* Exposing the Back of the Head or Spine - Exposing the back of the head or the spine during the bout, whether intentional or unintentional is strictly forbidden.  

* Intentionally Striking the Back of the Head Striking the back of the head or the spine during the bout, whether by intention or carelessness, is strictly forbidden.  The referee is expected to use their discretion in these instances, and may present the offending competitor with a yellow, red, or black card depending on the needs of the situation.   In the event that a fighter has turned away and is struck in the back of the head, both fighters may  receive a penalty at the referee’s discretion. 

* Equipment malfunction - An equipment malfunction is defined as a piece of equipment that is not working as intended, but is not broken or failed; For example, a competitors shin or elbow guard is loose and moves out of position requiring the bout to be halted for safety purposes until it is put back into its proper place.  After the second occurence of an equipment malfunction, the fencer will be yellow carded and has two minutes to fix or replace the item. Any subsequent occurrence in the bout the competitor will receive a second yellow card and have 2 minutes to fix or replace the malfunctioning equipment with a working item,

* Equipment Failure -  Equipment failure occurs when a required piece of equipment breaks or is no longer safe to use; e.g. a broken blade or a crumpled mask.  In the event of an equipment failure, the fencer will be given two minutes to replace their equipment. After two minutes they will receive a yellow card and an additional two minutes to replace the failed equipment, and present themselves at the ring ready to fence.  If the fencer has not returned after two minutes, they will be issued a second yellow card, and given an additional minute to return ready to fence.  If they have not returned after the final minute, or are unable to find a suitable piece of equipment to replace their weapon, they will be disqualified.  

* Competitor not present for start of bout -  At the start of the pool, or for any subsequent bout whether in the pool or in the elimination round, competitors must be present when called. Should they not be present they shall be disqualified with a Red card. There is a system in place to give fencers the time they need to prepare.

* Disrespectful Behavior - Competitors are expected to remain professional and in control of their emotions at all times, and to be respectful to all competitors, staff, participants, spectators, and the venue.  Disrespectful behavior includes the following (but not limited to):
** Disrespectful Vocalization: While fighters are allowed to vocally show excitement or frustration, taunting, swearing, etc., excessive celebrating to attempt to deceive the referees regarding whether a hit was made or failed to arrive, rude or snide comments to staff or competitors etc. are disallowed. 
** Arguing with the Referee or staff: Competitors and coaches are allowed to ask the referee to clarify the exchange if they did not understand the call or to dispute the enforcement of a rule if it has been incorrectly applied by the referee.  No arguing or disputing with the referee about a point of fact is allowed. 
** Loss of Temper: A fighter who is so angry that they cannot calm down, or is throwing down equipment in anger, etc. will be disqualified. 

* Disruption of the Scoring table -  Competitors, coaches, spectators, etc. are explicitly prohibited from interacting in any way with the scoring table during or between bouts.  Anyone who disregards this rule may be ejected from the event.  

* Unjustified interruption of the bout - Any competitor, coach, spectator, etc. who interrupts, disrupts or halts a bout without justification will be penalized and may be ejected from the event, including competitor leaving the bout at an unauthorized time. 

* Distracting the fencers - Only referees, other officials, or the competitors designated coach are allowed to speak to the competitors during the bout; this includes shouting advice to competitors, or attempting to distract or confuse a fighter.  Coaches are only allowed to shout advice to their own fighter.  

* Reckless behavior - Reckless and uncontrolled behavior is strictly prohibited.  This includes (but is not limited to) strike the floor due to loss of control, running out of the ring and into spectator areas or other fencing strips, shoving the opponent out of the ring, throwing the weapon, off-balancing oneself by running or leaping to attack, purposefully falling to attack, etc.   

* Intentionally Striking at Unauthorized Times - If a competitor intentionally attempts to hit an opponent when the bout is not in progress then the fighter will be disqualified; e.g. After “Halt” is called and the fighting has stopped, a fighter strikes the other fighter out of anger, malice, or for any other reason.  

* Use of prohibited techniques - This category includes any number of things, including (but not limited to) punching with a closed fist, percussive kicking (push kicks, i.e. pushing with the sole of the foot to the torso is allowed), attempting joint breaks, pain compliance, or chokes during grappling, holding the blade and striking with the cross guard and hilt (a “mordschlag”), etc. 

* Brutality - Competitors who strike or fight in such a way that is intended to cause injury, or who cause injury due to reckless behavior or excessive force, will be disqualified. Due to the nature of the activity, injuries can and will happen, and a competitor will not be penalized for an injury to the opponent unless it was due to brutality, a prohibited technique, or reckless behavior, etc.  
Examples of brutality include: drawing the fist back to punch (i.e. a “haymaker”), using the weapon or pommel or buckler to repeatedly bash the opponent, coiling up before striking a defenseless opponent, etc.  

* Cheating - Any participant found to be cheating will be disqualified and ejected from the tournament and event, and may be subject to a ban from future events.  Cheating includes but is not limited to: intentionally using disallowed equipment, removing required equipment after gear check, attempting to alter scoresheets, attempting to influence or coerce referees, etc.

* Use of Prohibited Substances - No participation in any fencing activity within the time limits for medication to leave the participant’s system, and in no case within 6 hours of using alcohol or any other prescription or nonprescription drug that may slow or impair actions. 

* Disputes/Appeals - If a competitor feels that a rule has been unfairly or incorrectly applied, an appeal may be made to the bout committee and head referee. No decision on a question of fact can be the subject of an appeal.  

== Notes:
